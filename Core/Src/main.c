/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "gfx.h"
#include "src/ginput/ginput_driver_mouse.h"
#include <string.h>
#include <stdio.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
static gImage myImage;
enum State {
	ST_PRESENTATION,
	ST_IDLE,
};
typedef enum State State;
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

TIM_HandleTypeDef htim3;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */
State state;


static GConsoleObject			gc;
static gFont					font;
static gCoord					bHeight;
static GHandle					ghc;
static gCoord					swidth, sheight;
static GListener gl; 			//para boton
static GHandle   ghButton1;		//para botón 1
static GHandle   ghButton2;		//para boton 2
static GHandle ghProgressbar;


float calibrationData[] = {
	0.06607,		// ax
	0.00106,		// bx
	-15.10857,		// cx
	-0.00023,		// ay
	0.08601,		// by
	-8.76305 		// cy
};

#if !GWIN_CONSOLE_USE_FLOAT
	#error "You need to enable float support for the console widget. (GWIN_CONSOLE_USE_FLOAT)"
#endif

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM3_Init(void);
static void MX_USART1_UART_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */


gBool LoadMouseCalibration(unsigned instance, void *data, gMemSize sz)
{
	(void)instance;

	if (sz != sizeof(calibrationData) || instance != 0) {
		return FALSE;
	}

	memcpy(data, (void*)&calibrationData, sz);

	return TRUE;
}

gTicks gfxSystemTicks(void)
{
	return HAL_GetTick();
}

gTicks gfxMillisecondsToTicks(gDelay ms)
{
	return ms;
}

// Para botón
static void createWidgets(void) {
	GWidgetInit	wi;

	// Apply some default values for GWIN
	gwinWidgetClearInit(&wi);
	wi.g.show = gTrue;

	// Apply the button parameters
	wi.g.x = 10;
	wi.g.y = 30;
	wi.g.width = 100;
	wi.g.height = 30;
	wi.text = "Decrementar";


	// Create the actual button
	ghButton1 = gwinButtonCreate(0, &wi);

	wi.g.x = 240-10-100;
	wi.text = "Incrementar";
	ghButton2 = gwinButtonCreate(0, &wi);

	GWidgetInit	wi2;

	wi2.customDraw = 0;
	wi2.customParam = 0;
	wi2.customStyle = 0;
	wi2.g.show = TRUE;

	wi2.g.x = 10;
	wi2.g.y = 100;
	wi2.g.width = 220;
	wi2.g.height = 20;
	wi2.text = "TEMPERATURA";
	ghProgressbar = gwinProgressbarCreate(NULL, &wi2);
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

	//para adc
	uint16_t raw;


  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC1_Init();
  MX_TIM3_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */

  // Initialize uGFX and the underlying system
  GEvent* pe;
	static const gOrientation	orients[] = { gOrientation0, gOrientation90, gOrientation180, gOrientation270 };
	unsigned which;

	gfxInit();	//inicio interrupción por botón

  state = ST_PRESENTATION;
  gCoord			swidth, sheight;

// Get the display dimensions
  swidth = gdispGetWidth();
  sheight = gdispGetHeight();

  // We are currently at gOrientation0
  	which = 0;
  	gdispSetOrientation(orients[which]);



  	// Set the widget defaults
  	//para botón
  	gwinSetDefaultFont(gdispOpenFont("UI2"));
  	gwinSetDefaultStyle(&WhiteWidgetStyle, gFalse);
  	gdispClear(GFX_WHITE);
  	gwinProgressbarSetResolution ( ghProgressbar ,  10 ) ;
  	// create the widget
  	createWidgets();

  	// We want to listen for widget events
  	geventListenerInit(&gl);
  	gwinAttachListener(&gl);


  	/*
  	  	//PARA CIRCULO
  	gCoord		width, height;

  	// PARA CIRCULO
  	width = gdispGetWidth();
  	height = gdispGetHeight();

 // Para circulo
  	gdispFillArc(width/2, height/2, width/4, -10, -45, GFX_WHITE);
	gdispDrawCircle(width/2+width/8, height/2-height/8, 13, GFX_GREEN);
	gdispFillCircle (240/2, 320/2, 40, GFX_RED);
	gdispDrawArc(width/2+width/8, height/2-height/8, 20, 25, 115, GFX_GRAY);
	gdispFillEllipse (width-width/6, height-height/6, width/8, height/16, GFX_BLUE);
	gdispDrawEllipse (width-width/6, height-height/6, width/16, height/8, GFX_YELLOW);
*/


#if 0
	// Create our title
	font = gdispOpenFont("UI2");
	gwinSetDefaultFont(font);
	bHeight = gdispGetFontMetric(font, gFontHeight)+4;
	gdispFillStringBox(0, 0, swidth, bHeight, "Touchscreen Calibration Grabber", font, GFX_RED, GFX_WHITE, gJustifyCenter);
	// Create our main display writing window
	{
		GWindowInit				wi;

		gwinClearInit(&wi);
		wi.show = gTrue; wi.x = 0; wi.y = bHeight; wi.width = swidth; wi.height = sheight-bHeight;
		ghc = gwinConsoleCreate(&gc, &wi);
	}

	// Get access to the GMouse structure
	m = (GMouse*)gdriverGetInstance(GDRIVER_TYPE_MOUSE, 0);
	if (!m) {
		gfxHalt("No mouse instance 0.");
	}


	// Print the calibration values
	gwinPrintf(ghc, "\r\n\n");
	gwinPrintf(ghc, "  ax: %f\r\n", m->caldata.ax);
	gwinPrintf(ghc, "  bx: %f\r\n", m->caldata.bx);
	gwinPrintf(ghc, "  cx: %f\r\n", m->caldata.cx);
	gwinPrintf(ghc, "  ay: %f\r\n", m->caldata.ay);
	gwinPrintf(ghc, "  by: %f\r\n", m->caldata.by);
	gwinPrintf(ghc, "  cy: %f\r\n", m->caldata.cy);
#endif



	#if 0
  gCoord		width, height;
    gCoord		i, j;

	// Get the screen size
  width = gdispGetWidth();
  height = gdispGetHeight();
	// Code Here
  gdispFillArc(width/2, height/2, width/4, -10, -45, GFX_WHITE);
  	gdispDrawCircle(width/2+width/8, height/2-height/8, 13, GFX_GREEN);
  	gdispFillCircle (width/2+width/8, height/2-height/8, 10, GFX_RED);
  	gdispDrawArc(width/2+width/8, height/2-height/8, 20, 25, 115, GFX_GRAY);
  	gdispFillEllipse (width-width/6, height-height/6, width/8, height/16, GFX_BLUE);
  	gdispDrawEllipse (width-width/6, height-height/6, width/16, height/8, GFX_YELLOW);

  	gCoord		y=0;
	gFont		font1, font2;
	gCoord		fheight1, fheight2;
	const char	*line1, *line2;
	char		buf[8];
  	line1 = "Esta es una prueba";
	line2 = "de la librería uGFX";

	font1 = gdispOpenFont("DejaVu*");
	fheight1 = gdispGetFontMetric(font1, gFontHeight)+2;
  		// Font 1
	gdispFillStringBox(0, y, width,  fheight1, line1, font1, GFX_BLACK, GFX_YELLOW, gJustifyCenter);
	y += fheight1+1;
	gdispFillStringBox(0, y, width,  fheight1, line2, font1, GFX_BLACK, GFX_YELLOW, gJustifyCenter);
	y += fheight1+1;
#endif
#if 0
	GHandle GW1, GW2;
	gdispClear(GFX_WHITE);

	    /* Create two windows */
	    {
	    	GWindowInit	wi;

			gwinClearInit(&wi);
	    	wi.show = gTrue; wi.x = 20; wi.y = 10; wi.width = 200; wi.height = 150;
	        GW1 = gwinWindowCreate(0, &wi);
	    	wi.show = gTrue; wi.x = 50; wi.y = 190; wi.width = 150; wi.height = 100;
	        GW2 = gwinWindowCreate(0, &wi);
	    }

	    /* Set fore- and background colors for both windows */
	    gwinSetColor(GW1, GFX_BLACK);
	    gwinSetBgColor(GW1, GFX_WHITE);
	    gwinSetColor(GW2, GFX_WHITE);
	    gwinSetBgColor(GW2, GFX_BLUE);

	    /* Clear both windows - to set background color */
	    gwinClear(GW1);
	    gwinClear(GW2);

	    gwinDrawLine(GW1, 5, 30, 150, 110);
	    for(i = 5, j = 0; i < 200 && j < 150; i += 3, j += i/20)
	        	gwinDrawPixel(GW1, i, j);

	    /*
	     * Draw two filled circles at the same coordinate
	     * of each window to demonstrate the relative coordinates
	     * of windows
	     */
	    gwinFillCircle(GW1, 20, 20, 15);
	    gwinFillCircle(GW2, 20, 20, 15);
#endif



  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {




    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

	  switch (state)
	  {
	  case ST_PRESENTATION:
		  	// Set up IO for our image
			gdispImageOpenFile(&myImage, "sveltia-logo-1550500491.bmp");
		  	gdispImageDraw(&myImage, (swidth-myImage.width)/2, (sheight-myImage.height)/2, swidth, sheight, 0, 0);
		  	gdispImageClose(&myImage);
		  	state = ST_IDLE;
		  	gdispClear(GFX_BLACK);
		  	gfxSleepMilliseconds(500);
		  break;
	  case ST_IDLE:
		  //para ADC
		  // Get ADC value
		  HAL_ADC_Start(&hadc1);
		  HAL_ADC_PollForConversion(&hadc1, HAL_MAX_DELAY);
		  raw = HAL_ADC_GetValue(&hadc1);

		  if (raw<180)
			 gwinProgressbarSetPosition(ghProgressbar, 0);
		  else if (raw<180)
			 gwinProgressbarSetPosition(ghProgressbar, 10);
		  else if (raw<360)
			 gwinProgressbarSetPosition(ghProgressbar, 20);
		  else if (raw<540)
			 gwinProgressbarSetPosition(ghProgressbar, 30);
		  else if (raw<720)
			 gwinProgressbarSetPosition(ghProgressbar, 40);
		  else if (raw<900)
		 	 gwinProgressbarSetPosition(ghProgressbar, 50);
	  	  else if (raw<1080)
	 		 gwinProgressbarSetPosition(ghProgressbar, 60);
	 	  else if (raw<1260)
			 gwinProgressbarSetPosition(ghProgressbar, 70);
		  else if (raw<1440)
			 gwinProgressbarSetPosition(ghProgressbar, 80);
		  else if (raw<1620)
		 	 gwinProgressbarSetPosition(ghProgressbar, 90);
		  else if (raw<1900)
			 gwinProgressbarSetPosition(ghProgressbar, 100);
		  // Get an Event
		  //evento del botón
		  pe = geventEventWait(&gl, 10);

		  switch(pe->type)
		  {
		  	  case GEVENT_GWIN_BUTTON:
		  		  if (((GEventGWinButton*)pe)->gwin == ghButton2)
		  			  {
		  			  		//CAMBIO DE ESTADO EL LED ROJO
		 	  				HAL_GPIO_TogglePin(GPIOB, LED_ROJO_Pin);
		 	  				uint8_t leo = 'A';
		 	  				HAL_UART_Transmit(&huart1, &leo, 1, 10);
		  			  }
		  		  else if (((GEventGWinButton*)pe)->gwin == ghButton1)
		  		  	  {
		 	  				//CAMBIO DE ESTADO EL LED ROJO
		 	  				HAL_GPIO_TogglePin(GPIOB, LED_VERDE_Pin);
		  		  	  }
		  break;
		  	  default:
		  break;
		  }
	break;
	  }




#if 0
	  					// Our button has been pressed
	  					if (++which >= sizeof(orients)/sizeof(orients[0]))
	  						which = 0;

	  					// Setting the orientation during run-time is a bit naughty particularly with
	  					// GWIN windows. In this case however we know that the button is in the top-left
	  					// corner which should translate safely into any orientation.
	  					gdispSetOrientation(orients[which]);
	  					gdispClear(GFX_WHITE);
	  					gwinRedrawDisplay(GDISP, gFalse);
#endif

	 //gfxSleepMilliseconds(1000);
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL5;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV4;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_11;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 4000-1;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 100-1;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5
                          |GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9
                          |GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13
                          |GPIO_PIN_14|GPIO_PIN_15|GPIO_PIN_0|GPIO_PIN_1, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, TOUCH_DIN_Pin|TOUCH_CS_Pin|TOUCH_CLK_Pin|BACKLIGHT_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, LCD_RESET_Pin|LCD_RD_Pin|LCD_WR_Pin|LCD_RS_Pin
                          |LCD_CS_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LED_ROJO_Pin|LED_VERDE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : PE2 PE3 PE4 PE5
                           PE6 PE7 PE8 PE9
                           PE10 PE11 PE12 PE13
                           PE14 PE15 PE0 PE1 */
  GPIO_InitStruct.Pin = GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5
                          |GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9
                          |GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13
                          |GPIO_PIN_14|GPIO_PIN_15|GPIO_PIN_0|GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : TOUCH_IRQ_Pin TOUCH_DOUT_Pin TOUCH_BUSY_Pin */
  GPIO_InitStruct.Pin = TOUCH_IRQ_Pin|TOUCH_DOUT_Pin|TOUCH_BUSY_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : TOUCH_DIN_Pin TOUCH_CS_Pin TOUCH_CLK_Pin LCD_RESET_Pin
                           LCD_RD_Pin LCD_WR_Pin LCD_RS_Pin LCD_CS_Pin */
  GPIO_InitStruct.Pin = TOUCH_DIN_Pin|TOUCH_CS_Pin|TOUCH_CLK_Pin|LCD_RESET_Pin
                          |LCD_RD_Pin|LCD_WR_Pin|LCD_RS_Pin|LCD_CS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : BACKLIGHT_Pin */
  GPIO_InitStruct.Pin = BACKLIGHT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(BACKLIGHT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LED_ROJO_Pin LED_VERDE_Pin */
  GPIO_InitStruct.Pin = LED_ROJO_Pin|LED_VERDE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
