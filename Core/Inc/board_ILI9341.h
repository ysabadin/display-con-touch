/*
 * This file is subject to the terms of the GFX License. If a copy of
 * the license was not distributed with this file, you can obtain one at:
 *
 *              http://ugfx.io/license.html
 */

#ifndef _GDISP_LLD_BOARD_H
#define _GDISP_LLD_BOARD_H

#include "stm32f1xx_hal.h"


#define GPIO_TYPE  GPIOD
#define LED      7
#define LCD_CS   4
#define LCD_RS   3
#define LCD_RST  0
#define LCD_WR   2
#define LCD_RD   1
#define LCD_BL	 6
// For a multiple display configuration we would put all this in a structure and then
//	set g->board to that structure.
#define SET_CS    GPIO_TYPE->BSRR=1<<LCD_CS;
#define CLR_CS    GPIO_TYPE->BRR=1<<LCD_CS;
#define SET_RS    GPIO_TYPE->BSRR=1<<LCD_RS;
#define CLR_RS    GPIO_TYPE->BRR=1<<LCD_RS;
#define SET_WR    GPIO_TYPE->BSRR=1<<LCD_WR;
#define CLR_WR    GPIO_TYPE->BRR=1<<LCD_WR;
#define SET_RD    GPIO_TYPE->BSRR=1<<LCD_RD;
#define CLR_RD    GPIO_TYPE->BRR=1<<LCD_RD;
#define SET_BL    GPIO_TYPE->BSRR=1<<LCD_BL;
#define CLR_BL    GPIO_TYPE->BRR=1<<LCD_BL;

#define	LCD_RST_SET	GPIO_TYPE->BSRR=1<<LCD_RST;
#define	LCD_RST_CLR	GPIO_TYPE->BRR=1<<LCD_RST;    //��λ


static GFXINLINE void init_board(GDisplay *g) {

	// As we are not using multiple displays we set g->board to NULL as we don't use it.
	g->board = 0;

	switch(g->controllerdisplay) {
	case 0:											// Set up for Display 0
		/* Configure the pins to a well know state */
		SET_RS;
		SET_RD;
		SET_WR;
		CLR_CS;
		break;
	}
}

static GFXINLINE void post_init_board(GDisplay *g) {
	(void) g;
}

static GFXINLINE void setpin_reset(GDisplay *g, gBool state) {
	(void) g;
	if(state) {
		// reset lcd
		LCD_RST_CLR
	} else {
		LCD_RST_SET
	}
}

static GFXINLINE void set_backlight(GDisplay *g, gU8 percent) {
	(void) g;
	// TODO: can probably pwm this
	if(percent) {
		// turn back light on
		SET_BL;
	} else {
		// turn off
		CLR_BL;
	}
}

static GFXINLINE void acquire_bus(GDisplay *g) {
	(void) g;
}

static GFXINLINE void release_bus(GDisplay *g) {
	(void) g;
}

/**
 * @brief   Short delay
 *
 * @param[in] dly		Length of delay
 *
 * @notapi
 */
static GFXINLINE void ili9341_delay(gU16 dly) {
  static gU16 i;
  for(i = 0; i < dly; i++)
    asm("nop");
}

static GFXINLINE void write_index(GDisplay *g, gU16 index) {
	(void) g;
	GPIOE->ODR = index;
	CLR_RS; CLR_WR; ili9341_delay(1); SET_WR; ili9341_delay(1); SET_RS;
}

static GFXINLINE void write_data(GDisplay *g, gU16 data) {
	(void) g;
	GPIOE->ODR = data;
	CLR_WR; ili9341_delay(1); SET_WR; ili9341_delay(1);
}

static GFXINLINE void setreadmode(GDisplay *g) {
	(void) g;
	// change pin mode to digital input
//	palSetGroupMode(GPIOE, PAL_WHOLE_PORT, 0, PAL_MODE_INPUT);
}

static GFXINLINE void setwritemode(GDisplay *g) {
	(void) g;
	// change pin mode back to digital output
//	palSetGroupMode(GPIOE, PAL_WHOLE_PORT, 0, PAL_MODE_OUTPUT_PUSHPULL);
}

static GFXINLINE gU16 read_data(GDisplay *g) {
	gU16	value = 0;
	(void) g;
	CLR_RD;
//	value = palReadPort(GPIOE);
//	value = palReadPort(GPIOE);
	SET_RD;
	return value;
}

#endif /* _GDISP_LLD_BOARD_H */
