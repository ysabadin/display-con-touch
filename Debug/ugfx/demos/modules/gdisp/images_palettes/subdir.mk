################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ugfx/demos/modules/gdisp/images_palettes/main.c 

OBJS += \
./ugfx/demos/modules/gdisp/images_palettes/main.o 

C_DEPS += \
./ugfx/demos/modules/gdisp/images_palettes/main.d 


# Each subdirectory must supply rules for building sources it contributes
ugfx/demos/modules/gdisp/images_palettes/%.o: ../ugfx/demos/modules/gdisp/images_palettes/%.c ugfx/demos/modules/gdisp/images_palettes/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"/home/lgiordano/workspace/cube_tests/sveltia_3/ugfx" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

