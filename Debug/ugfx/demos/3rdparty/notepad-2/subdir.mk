################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ugfx/demos/3rdparty/notepad-2/main.c \
../ugfx/demos/3rdparty/notepad-2/notepadApp.c \
../ugfx/demos/3rdparty/notepad-2/notepadCore.c 

OBJS += \
./ugfx/demos/3rdparty/notepad-2/main.o \
./ugfx/demos/3rdparty/notepad-2/notepadApp.o \
./ugfx/demos/3rdparty/notepad-2/notepadCore.o 

C_DEPS += \
./ugfx/demos/3rdparty/notepad-2/main.d \
./ugfx/demos/3rdparty/notepad-2/notepadApp.d \
./ugfx/demos/3rdparty/notepad-2/notepadCore.d 


# Each subdirectory must supply rules for building sources it contributes
ugfx/demos/3rdparty/notepad-2/%.o: ../ugfx/demos/3rdparty/notepad-2/%.c ugfx/demos/3rdparty/notepad-2/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"/home/lgiordano/workspace/cube_tests/sveltia_3/ugfx" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

