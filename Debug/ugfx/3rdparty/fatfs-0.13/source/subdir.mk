################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ugfx/3rdparty/fatfs-0.13/source/diskio.c \
../ugfx/3rdparty/fatfs-0.13/source/ff.c \
../ugfx/3rdparty/fatfs-0.13/source/ffsystem.c \
../ugfx/3rdparty/fatfs-0.13/source/ffunicode.c 

OBJS += \
./ugfx/3rdparty/fatfs-0.13/source/diskio.o \
./ugfx/3rdparty/fatfs-0.13/source/ff.o \
./ugfx/3rdparty/fatfs-0.13/source/ffsystem.o \
./ugfx/3rdparty/fatfs-0.13/source/ffunicode.o 

C_DEPS += \
./ugfx/3rdparty/fatfs-0.13/source/diskio.d \
./ugfx/3rdparty/fatfs-0.13/source/ff.d \
./ugfx/3rdparty/fatfs-0.13/source/ffsystem.d \
./ugfx/3rdparty/fatfs-0.13/source/ffunicode.d 


# Each subdirectory must supply rules for building sources it contributes
ugfx/3rdparty/fatfs-0.13/source/%.o: ../ugfx/3rdparty/fatfs-0.13/source/%.c ugfx/3rdparty/fatfs-0.13/source/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"/home/lgiordano/workspace/cube_tests/sveltia_3/ugfx" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

