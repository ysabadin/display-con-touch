################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ugfx/3rdparty/tinygl-0.4-ugfx/src/api.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/src/arrays.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/src/clear.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/src/clip.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/src/error.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/src/get.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/src/glx.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/src/image_util.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/src/init.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/src/light.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/src/list.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/src/matrix.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/src/memory.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/src/misc.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/src/msghandling.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/src/nglx.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/src/oscontext.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/src/select.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/src/specbuf.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/src/texture.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/src/vertex.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/src/zbuffer.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/src/zdither.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/src/zline.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/src/zmath.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/src/ztriangle.c 

OBJS += \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/api.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/arrays.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/clear.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/clip.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/error.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/get.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/glx.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/image_util.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/init.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/light.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/list.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/matrix.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/memory.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/misc.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/msghandling.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/nglx.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/oscontext.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/select.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/specbuf.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/texture.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/vertex.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/zbuffer.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/zdither.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/zline.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/zmath.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/ztriangle.o 

C_DEPS += \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/api.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/arrays.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/clear.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/clip.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/error.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/get.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/glx.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/image_util.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/init.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/light.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/list.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/matrix.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/memory.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/misc.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/msghandling.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/nglx.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/oscontext.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/select.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/specbuf.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/texture.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/vertex.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/zbuffer.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/zdither.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/zline.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/zmath.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/src/ztriangle.d 


# Each subdirectory must supply rules for building sources it contributes
ugfx/3rdparty/tinygl-0.4-ugfx/src/%.o: ../ugfx/3rdparty/tinygl-0.4-ugfx/src/%.c ugfx/3rdparty/tinygl-0.4-ugfx/src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"/home/lgiordano/workspace/cube_tests/sveltia_3/ugfx" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

