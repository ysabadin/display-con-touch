################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ugfx/3rdparty/tinygl-0.4-ugfx/examples/gears.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/examples/glu.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/examples/mech.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/examples/nanox.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/examples/spin.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/examples/texobj.c \
../ugfx/3rdparty/tinygl-0.4-ugfx/examples/x11.c 

OBJS += \
./ugfx/3rdparty/tinygl-0.4-ugfx/examples/gears.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/examples/glu.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/examples/mech.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/examples/nanox.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/examples/spin.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/examples/texobj.o \
./ugfx/3rdparty/tinygl-0.4-ugfx/examples/x11.o 

C_DEPS += \
./ugfx/3rdparty/tinygl-0.4-ugfx/examples/gears.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/examples/glu.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/examples/mech.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/examples/nanox.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/examples/spin.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/examples/texobj.d \
./ugfx/3rdparty/tinygl-0.4-ugfx/examples/x11.d 


# Each subdirectory must supply rules for building sources it contributes
ugfx/3rdparty/tinygl-0.4-ugfx/examples/%.o: ../ugfx/3rdparty/tinygl-0.4-ugfx/examples/%.c ugfx/3rdparty/tinygl-0.4-ugfx/examples/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"/home/lgiordano/workspace/cube_tests/sveltia_3/ugfx" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

