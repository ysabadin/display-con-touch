################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ugfx/gdisp/fonts/DejaVuSans10.c \
../ugfx/gdisp/fonts/DejaVuSans12.c \
../ugfx/gdisp/fonts/DejaVuSans12_aa.c \
../ugfx/gdisp/fonts/DejaVuSans16.c \
../ugfx/gdisp/fonts/DejaVuSans16_aa.c \
../ugfx/gdisp/fonts/DejaVuSans20.c \
../ugfx/gdisp/fonts/DejaVuSans20_aa.c \
../ugfx/gdisp/fonts/DejaVuSans24.c \
../ugfx/gdisp/fonts/DejaVuSans24_aa.c \
../ugfx/gdisp/fonts/DejaVuSans32.c \
../ugfx/gdisp/fonts/DejaVuSans32_aa.c \
../ugfx/gdisp/fonts/DejaVuSansBold12.c \
../ugfx/gdisp/fonts/DejaVuSansBold12_aa.c \
../ugfx/gdisp/fonts/LargeNumbers.c \
../ugfx/gdisp/fonts/UI1.c \
../ugfx/gdisp/fonts/UI2.c \
../ugfx/gdisp/fonts/fixed_10x20.c \
../ugfx/gdisp/fonts/fixed_5x8.c \
../ugfx/gdisp/fonts/fixed_7x14.c 

OBJS += \
./ugfx/gdisp/fonts/DejaVuSans10.o \
./ugfx/gdisp/fonts/DejaVuSans12.o \
./ugfx/gdisp/fonts/DejaVuSans12_aa.o \
./ugfx/gdisp/fonts/DejaVuSans16.o \
./ugfx/gdisp/fonts/DejaVuSans16_aa.o \
./ugfx/gdisp/fonts/DejaVuSans20.o \
./ugfx/gdisp/fonts/DejaVuSans20_aa.o \
./ugfx/gdisp/fonts/DejaVuSans24.o \
./ugfx/gdisp/fonts/DejaVuSans24_aa.o \
./ugfx/gdisp/fonts/DejaVuSans32.o \
./ugfx/gdisp/fonts/DejaVuSans32_aa.o \
./ugfx/gdisp/fonts/DejaVuSansBold12.o \
./ugfx/gdisp/fonts/DejaVuSansBold12_aa.o \
./ugfx/gdisp/fonts/LargeNumbers.o \
./ugfx/gdisp/fonts/UI1.o \
./ugfx/gdisp/fonts/UI2.o \
./ugfx/gdisp/fonts/fixed_10x20.o \
./ugfx/gdisp/fonts/fixed_5x8.o \
./ugfx/gdisp/fonts/fixed_7x14.o 

C_DEPS += \
./ugfx/gdisp/fonts/DejaVuSans10.d \
./ugfx/gdisp/fonts/DejaVuSans12.d \
./ugfx/gdisp/fonts/DejaVuSans12_aa.d \
./ugfx/gdisp/fonts/DejaVuSans16.d \
./ugfx/gdisp/fonts/DejaVuSans16_aa.d \
./ugfx/gdisp/fonts/DejaVuSans20.d \
./ugfx/gdisp/fonts/DejaVuSans20_aa.d \
./ugfx/gdisp/fonts/DejaVuSans24.d \
./ugfx/gdisp/fonts/DejaVuSans24_aa.d \
./ugfx/gdisp/fonts/DejaVuSans32.d \
./ugfx/gdisp/fonts/DejaVuSans32_aa.d \
./ugfx/gdisp/fonts/DejaVuSansBold12.d \
./ugfx/gdisp/fonts/DejaVuSansBold12_aa.d \
./ugfx/gdisp/fonts/LargeNumbers.d \
./ugfx/gdisp/fonts/UI1.d \
./ugfx/gdisp/fonts/UI2.d \
./ugfx/gdisp/fonts/fixed_10x20.d \
./ugfx/gdisp/fonts/fixed_5x8.d \
./ugfx/gdisp/fonts/fixed_7x14.d 


# Each subdirectory must supply rules for building sources it contributes
ugfx/gdisp/fonts/%.o: ../ugfx/gdisp/fonts/%.c ugfx/gdisp/fonts/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"/home/lgiordano/workspace/cube_tests/sveltia_3/ugfx" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

