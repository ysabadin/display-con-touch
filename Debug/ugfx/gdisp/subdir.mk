################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ugfx/gdisp/gdisp.c \
../ugfx/gdisp/gdisp_fonts.c \
../ugfx/gdisp/gdisp_image.c \
../ugfx/gdisp/gdisp_image_bmp.c \
../ugfx/gdisp/gdisp_image_gif.c \
../ugfx/gdisp/gdisp_image_jpg.c \
../ugfx/gdisp/gdisp_image_native.c \
../ugfx/gdisp/gdisp_image_png.c \
../ugfx/gdisp/gdisp_mk.c \
../ugfx/gdisp/gdisp_pixmap.c 

OBJS += \
./ugfx/gdisp/gdisp.o \
./ugfx/gdisp/gdisp_fonts.o \
./ugfx/gdisp/gdisp_image.o \
./ugfx/gdisp/gdisp_image_bmp.o \
./ugfx/gdisp/gdisp_image_gif.o \
./ugfx/gdisp/gdisp_image_jpg.o \
./ugfx/gdisp/gdisp_image_native.o \
./ugfx/gdisp/gdisp_image_png.o \
./ugfx/gdisp/gdisp_mk.o \
./ugfx/gdisp/gdisp_pixmap.o 

C_DEPS += \
./ugfx/gdisp/gdisp.d \
./ugfx/gdisp/gdisp_fonts.d \
./ugfx/gdisp/gdisp_image.d \
./ugfx/gdisp/gdisp_image_bmp.d \
./ugfx/gdisp/gdisp_image_gif.d \
./ugfx/gdisp/gdisp_image_jpg.d \
./ugfx/gdisp/gdisp_image_native.d \
./ugfx/gdisp/gdisp_image_png.d \
./ugfx/gdisp/gdisp_mk.d \
./ugfx/gdisp/gdisp_pixmap.d 


# Each subdirectory must supply rules for building sources it contributes
ugfx/gdisp/%.o: ../ugfx/gdisp/%.c ugfx/gdisp/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"/home/lgiordano/workspace/cube_tests/sveltia_3/ugfx" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

