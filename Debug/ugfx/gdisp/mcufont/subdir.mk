################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ugfx/gdisp/mcufont/mf_bwfont.c \
../ugfx/gdisp/mcufont/mf_encoding.c \
../ugfx/gdisp/mcufont/mf_font.c \
../ugfx/gdisp/mcufont/mf_justify.c \
../ugfx/gdisp/mcufont/mf_kerning.c \
../ugfx/gdisp/mcufont/mf_rlefont.c \
../ugfx/gdisp/mcufont/mf_scaledfont.c \
../ugfx/gdisp/mcufont/mf_wordwrap.c 

OBJS += \
./ugfx/gdisp/mcufont/mf_bwfont.o \
./ugfx/gdisp/mcufont/mf_encoding.o \
./ugfx/gdisp/mcufont/mf_font.o \
./ugfx/gdisp/mcufont/mf_justify.o \
./ugfx/gdisp/mcufont/mf_kerning.o \
./ugfx/gdisp/mcufont/mf_rlefont.o \
./ugfx/gdisp/mcufont/mf_scaledfont.o \
./ugfx/gdisp/mcufont/mf_wordwrap.o 

C_DEPS += \
./ugfx/gdisp/mcufont/mf_bwfont.d \
./ugfx/gdisp/mcufont/mf_encoding.d \
./ugfx/gdisp/mcufont/mf_font.d \
./ugfx/gdisp/mcufont/mf_justify.d \
./ugfx/gdisp/mcufont/mf_kerning.d \
./ugfx/gdisp/mcufont/mf_rlefont.d \
./ugfx/gdisp/mcufont/mf_scaledfont.d \
./ugfx/gdisp/mcufont/mf_wordwrap.d 


# Each subdirectory must supply rules for building sources it contributes
ugfx/gdisp/mcufont/%.o: ../ugfx/gdisp/mcufont/%.c ugfx/gdisp/mcufont/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"/home/lgiordano/workspace/cube_tests/sveltia_3/ugfx" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

