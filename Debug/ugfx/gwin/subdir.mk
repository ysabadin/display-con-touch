################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ugfx/gwin/gwin.c \
../ugfx/gwin/gwin_button.c \
../ugfx/gwin/gwin_checkbox.c \
../ugfx/gwin/gwin_console.c \
../ugfx/gwin/gwin_container.c \
../ugfx/gwin/gwin_frame.c \
../ugfx/gwin/gwin_gl3d.c \
../ugfx/gwin/gwin_graph.c \
../ugfx/gwin/gwin_image.c \
../ugfx/gwin/gwin_keyboard.c \
../ugfx/gwin/gwin_keyboard_layout.c \
../ugfx/gwin/gwin_label.c \
../ugfx/gwin/gwin_list.c \
../ugfx/gwin/gwin_mk.c \
../ugfx/gwin/gwin_progressbar.c \
../ugfx/gwin/gwin_radio.c \
../ugfx/gwin/gwin_slider.c \
../ugfx/gwin/gwin_tabset.c \
../ugfx/gwin/gwin_textedit.c \
../ugfx/gwin/gwin_widget.c \
../ugfx/gwin/gwin_wm.c 

OBJS += \
./ugfx/gwin/gwin.o \
./ugfx/gwin/gwin_button.o \
./ugfx/gwin/gwin_checkbox.o \
./ugfx/gwin/gwin_console.o \
./ugfx/gwin/gwin_container.o \
./ugfx/gwin/gwin_frame.o \
./ugfx/gwin/gwin_gl3d.o \
./ugfx/gwin/gwin_graph.o \
./ugfx/gwin/gwin_image.o \
./ugfx/gwin/gwin_keyboard.o \
./ugfx/gwin/gwin_keyboard_layout.o \
./ugfx/gwin/gwin_label.o \
./ugfx/gwin/gwin_list.o \
./ugfx/gwin/gwin_mk.o \
./ugfx/gwin/gwin_progressbar.o \
./ugfx/gwin/gwin_radio.o \
./ugfx/gwin/gwin_slider.o \
./ugfx/gwin/gwin_tabset.o \
./ugfx/gwin/gwin_textedit.o \
./ugfx/gwin/gwin_widget.o \
./ugfx/gwin/gwin_wm.o 

C_DEPS += \
./ugfx/gwin/gwin.d \
./ugfx/gwin/gwin_button.d \
./ugfx/gwin/gwin_checkbox.d \
./ugfx/gwin/gwin_console.d \
./ugfx/gwin/gwin_container.d \
./ugfx/gwin/gwin_frame.d \
./ugfx/gwin/gwin_gl3d.d \
./ugfx/gwin/gwin_graph.d \
./ugfx/gwin/gwin_image.d \
./ugfx/gwin/gwin_keyboard.d \
./ugfx/gwin/gwin_keyboard_layout.d \
./ugfx/gwin/gwin_label.d \
./ugfx/gwin/gwin_list.d \
./ugfx/gwin/gwin_mk.d \
./ugfx/gwin/gwin_progressbar.d \
./ugfx/gwin/gwin_radio.d \
./ugfx/gwin/gwin_slider.d \
./ugfx/gwin/gwin_tabset.d \
./ugfx/gwin/gwin_textedit.d \
./ugfx/gwin/gwin_widget.d \
./ugfx/gwin/gwin_wm.d 


# Each subdirectory must supply rules for building sources it contributes
ugfx/gwin/%.o: ../ugfx/gwin/%.c ugfx/gwin/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"/home/lgiordano/workspace/cube_tests/sveltia_3/ugfx" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

