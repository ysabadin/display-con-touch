################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ugfx/ginput/ginput.c \
../ugfx/ginput/ginput_dial.c \
../ugfx/ginput/ginput_keyboard.c \
../ugfx/ginput/ginput_keyboard_microcode.c \
../ugfx/ginput/ginput_mk.c \
../ugfx/ginput/ginput_mouse.c \
../ugfx/ginput/ginput_toggle.c 

OBJS += \
./ugfx/ginput/ginput.o \
./ugfx/ginput/ginput_dial.o \
./ugfx/ginput/ginput_keyboard.o \
./ugfx/ginput/ginput_keyboard_microcode.o \
./ugfx/ginput/ginput_mk.o \
./ugfx/ginput/ginput_mouse.o \
./ugfx/ginput/ginput_toggle.o 

C_DEPS += \
./ugfx/ginput/ginput.d \
./ugfx/ginput/ginput_dial.d \
./ugfx/ginput/ginput_keyboard.d \
./ugfx/ginput/ginput_keyboard_microcode.d \
./ugfx/ginput/ginput_mk.d \
./ugfx/ginput/ginput_mouse.d \
./ugfx/ginput/ginput_toggle.d 


# Each subdirectory must supply rules for building sources it contributes
ugfx/ginput/%.o: ../ugfx/ginput/%.c ugfx/ginput/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"/home/lgiordano/workspace/cube_tests/sveltia_3/ugfx" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

