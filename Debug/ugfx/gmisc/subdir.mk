################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ugfx/gmisc/gmisc.c \
../ugfx/gmisc/gmisc_arrayops.c \
../ugfx/gmisc/gmisc_hittest.c \
../ugfx/gmisc/gmisc_matrix2d.c \
../ugfx/gmisc/gmisc_mk.c \
../ugfx/gmisc/gmisc_trig.c 

OBJS += \
./ugfx/gmisc/gmisc.o \
./ugfx/gmisc/gmisc_arrayops.o \
./ugfx/gmisc/gmisc_hittest.o \
./ugfx/gmisc/gmisc_matrix2d.o \
./ugfx/gmisc/gmisc_mk.o \
./ugfx/gmisc/gmisc_trig.o 

C_DEPS += \
./ugfx/gmisc/gmisc.d \
./ugfx/gmisc/gmisc_arrayops.d \
./ugfx/gmisc/gmisc_hittest.d \
./ugfx/gmisc/gmisc_matrix2d.d \
./ugfx/gmisc/gmisc_mk.d \
./ugfx/gmisc/gmisc_trig.d 


# Each subdirectory must supply rules for building sources it contributes
ugfx/gmisc/%.o: ../ugfx/gmisc/%.c ugfx/gmisc/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"/home/lgiordano/workspace/cube_tests/sveltia_3/ugfx" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

