################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ugfx/gos/gos_arduino.c \
../ugfx/gos/gos_chibios.c \
../ugfx/gos/gos_cmsis.c \
../ugfx/gos/gos_cmsis2.c \
../ugfx/gos/gos_ecos.c \
../ugfx/gos/gos_freertos.c \
../ugfx/gos/gos_linux.c \
../ugfx/gos/gos_mk.c \
../ugfx/gos/gos_nios.c \
../ugfx/gos/gos_osx.c \
../ugfx/gos/gos_raw32.c \
../ugfx/gos/gos_rawrtos.c \
../ugfx/gos/gos_win32.c \
../ugfx/gos/gos_x_heap.c \
../ugfx/gos/gos_x_threads.c \
../ugfx/gos/gos_zephyr.c 

OBJS += \
./ugfx/gos/gos_arduino.o \
./ugfx/gos/gos_chibios.o \
./ugfx/gos/gos_cmsis.o \
./ugfx/gos/gos_cmsis2.o \
./ugfx/gos/gos_ecos.o \
./ugfx/gos/gos_freertos.o \
./ugfx/gos/gos_linux.o \
./ugfx/gos/gos_mk.o \
./ugfx/gos/gos_nios.o \
./ugfx/gos/gos_osx.o \
./ugfx/gos/gos_raw32.o \
./ugfx/gos/gos_rawrtos.o \
./ugfx/gos/gos_win32.o \
./ugfx/gos/gos_x_heap.o \
./ugfx/gos/gos_x_threads.o \
./ugfx/gos/gos_zephyr.o 

C_DEPS += \
./ugfx/gos/gos_arduino.d \
./ugfx/gos/gos_chibios.d \
./ugfx/gos/gos_cmsis.d \
./ugfx/gos/gos_cmsis2.d \
./ugfx/gos/gos_ecos.d \
./ugfx/gos/gos_freertos.d \
./ugfx/gos/gos_linux.d \
./ugfx/gos/gos_mk.d \
./ugfx/gos/gos_nios.d \
./ugfx/gos/gos_osx.d \
./ugfx/gos/gos_raw32.d \
./ugfx/gos/gos_rawrtos.d \
./ugfx/gos/gos_win32.d \
./ugfx/gos/gos_x_heap.d \
./ugfx/gos/gos_x_threads.d \
./ugfx/gos/gos_zephyr.d 


# Each subdirectory must supply rules for building sources it contributes
ugfx/gos/%.o: ../ugfx/gos/%.c ugfx/gos/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"/home/lgiordano/workspace/cube_tests/sveltia_3/ugfx" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

