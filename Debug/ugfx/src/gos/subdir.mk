################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ugfx/src/gos/gos_arduino.c \
../ugfx/src/gos/gos_chibios.c \
../ugfx/src/gos/gos_cmsis.c \
../ugfx/src/gos/gos_cmsis2.c \
../ugfx/src/gos/gos_ecos.c \
../ugfx/src/gos/gos_freertos.c \
../ugfx/src/gos/gos_linux.c \
../ugfx/src/gos/gos_mk.c \
../ugfx/src/gos/gos_nios.c \
../ugfx/src/gos/gos_osx.c \
../ugfx/src/gos/gos_raw32.c \
../ugfx/src/gos/gos_rawrtos.c \
../ugfx/src/gos/gos_win32.c \
../ugfx/src/gos/gos_x_heap.c \
../ugfx/src/gos/gos_x_threads.c \
../ugfx/src/gos/gos_zephyr.c 

OBJS += \
./ugfx/src/gos/gos_arduino.o \
./ugfx/src/gos/gos_chibios.o \
./ugfx/src/gos/gos_cmsis.o \
./ugfx/src/gos/gos_cmsis2.o \
./ugfx/src/gos/gos_ecos.o \
./ugfx/src/gos/gos_freertos.o \
./ugfx/src/gos/gos_linux.o \
./ugfx/src/gos/gos_mk.o \
./ugfx/src/gos/gos_nios.o \
./ugfx/src/gos/gos_osx.o \
./ugfx/src/gos/gos_raw32.o \
./ugfx/src/gos/gos_rawrtos.o \
./ugfx/src/gos/gos_win32.o \
./ugfx/src/gos/gos_x_heap.o \
./ugfx/src/gos/gos_x_threads.o \
./ugfx/src/gos/gos_zephyr.o 

C_DEPS += \
./ugfx/src/gos/gos_arduino.d \
./ugfx/src/gos/gos_chibios.d \
./ugfx/src/gos/gos_cmsis.d \
./ugfx/src/gos/gos_cmsis2.d \
./ugfx/src/gos/gos_ecos.d \
./ugfx/src/gos/gos_freertos.d \
./ugfx/src/gos/gos_linux.d \
./ugfx/src/gos/gos_mk.d \
./ugfx/src/gos/gos_nios.d \
./ugfx/src/gos/gos_osx.d \
./ugfx/src/gos/gos_raw32.d \
./ugfx/src/gos/gos_rawrtos.d \
./ugfx/src/gos/gos_win32.d \
./ugfx/src/gos/gos_x_heap.d \
./ugfx/src/gos/gos_x_threads.d \
./ugfx/src/gos/gos_zephyr.d 


# Each subdirectory must supply rules for building sources it contributes
ugfx/src/gos/%.o: ../ugfx/src/gos/%.c ugfx/src/gos/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"/home/lgiordano/workspace/cube_tests/sveltia_3/ugfx" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

