################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ugfx/src/gfile/gfile.c \
../ugfx/src/gfile/gfile_fatfs_diskio_chibios.c \
../ugfx/src/gfile/gfile_fatfs_wrapper.c \
../ugfx/src/gfile/gfile_fs_chibios.c \
../ugfx/src/gfile/gfile_fs_fatfs.c \
../ugfx/src/gfile/gfile_fs_mem.c \
../ugfx/src/gfile/gfile_fs_native.c \
../ugfx/src/gfile/gfile_fs_petitfs.c \
../ugfx/src/gfile/gfile_fs_ram.c \
../ugfx/src/gfile/gfile_fs_rom.c \
../ugfx/src/gfile/gfile_fs_strings.c \
../ugfx/src/gfile/gfile_mk.c \
../ugfx/src/gfile/gfile_petitfs_diskio_chibios.c \
../ugfx/src/gfile/gfile_petitfs_wrapper.c \
../ugfx/src/gfile/gfile_printg.c \
../ugfx/src/gfile/gfile_scang.c \
../ugfx/src/gfile/gfile_stdio.c 

OBJS += \
./ugfx/src/gfile/gfile.o \
./ugfx/src/gfile/gfile_fatfs_diskio_chibios.o \
./ugfx/src/gfile/gfile_fatfs_wrapper.o \
./ugfx/src/gfile/gfile_fs_chibios.o \
./ugfx/src/gfile/gfile_fs_fatfs.o \
./ugfx/src/gfile/gfile_fs_mem.o \
./ugfx/src/gfile/gfile_fs_native.o \
./ugfx/src/gfile/gfile_fs_petitfs.o \
./ugfx/src/gfile/gfile_fs_ram.o \
./ugfx/src/gfile/gfile_fs_rom.o \
./ugfx/src/gfile/gfile_fs_strings.o \
./ugfx/src/gfile/gfile_mk.o \
./ugfx/src/gfile/gfile_petitfs_diskio_chibios.o \
./ugfx/src/gfile/gfile_petitfs_wrapper.o \
./ugfx/src/gfile/gfile_printg.o \
./ugfx/src/gfile/gfile_scang.o \
./ugfx/src/gfile/gfile_stdio.o 

C_DEPS += \
./ugfx/src/gfile/gfile.d \
./ugfx/src/gfile/gfile_fatfs_diskio_chibios.d \
./ugfx/src/gfile/gfile_fatfs_wrapper.d \
./ugfx/src/gfile/gfile_fs_chibios.d \
./ugfx/src/gfile/gfile_fs_fatfs.d \
./ugfx/src/gfile/gfile_fs_mem.d \
./ugfx/src/gfile/gfile_fs_native.d \
./ugfx/src/gfile/gfile_fs_petitfs.d \
./ugfx/src/gfile/gfile_fs_ram.d \
./ugfx/src/gfile/gfile_fs_rom.d \
./ugfx/src/gfile/gfile_fs_strings.d \
./ugfx/src/gfile/gfile_mk.d \
./ugfx/src/gfile/gfile_petitfs_diskio_chibios.d \
./ugfx/src/gfile/gfile_petitfs_wrapper.d \
./ugfx/src/gfile/gfile_printg.d \
./ugfx/src/gfile/gfile_scang.d \
./ugfx/src/gfile/gfile_stdio.d 


# Each subdirectory must supply rules for building sources it contributes
ugfx/src/gfile/%.o: ../ugfx/src/gfile/%.c ugfx/src/gfile/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"/home/lgiordano/workspace/cube_tests/sveltia_3/ugfx" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

