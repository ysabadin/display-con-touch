################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ugfx/src/ginput/ginput.c \
../ugfx/src/ginput/ginput_dial.c \
../ugfx/src/ginput/ginput_keyboard.c \
../ugfx/src/ginput/ginput_keyboard_microcode.c \
../ugfx/src/ginput/ginput_mk.c \
../ugfx/src/ginput/ginput_mouse.c \
../ugfx/src/ginput/ginput_toggle.c 

OBJS += \
./ugfx/src/ginput/ginput.o \
./ugfx/src/ginput/ginput_dial.o \
./ugfx/src/ginput/ginput_keyboard.o \
./ugfx/src/ginput/ginput_keyboard_microcode.o \
./ugfx/src/ginput/ginput_mk.o \
./ugfx/src/ginput/ginput_mouse.o \
./ugfx/src/ginput/ginput_toggle.o 

C_DEPS += \
./ugfx/src/ginput/ginput.d \
./ugfx/src/ginput/ginput_dial.d \
./ugfx/src/ginput/ginput_keyboard.d \
./ugfx/src/ginput/ginput_keyboard_microcode.d \
./ugfx/src/ginput/ginput_mk.d \
./ugfx/src/ginput/ginput_mouse.d \
./ugfx/src/ginput/ginput_toggle.d 


# Each subdirectory must supply rules for building sources it contributes
ugfx/src/ginput/%.o: ../ugfx/src/ginput/%.c ugfx/src/ginput/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"/home/lgiordano/workspace/cube_tests/sveltia_3/ugfx" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

