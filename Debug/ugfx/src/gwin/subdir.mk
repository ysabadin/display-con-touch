################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ugfx/src/gwin/gwin.c \
../ugfx/src/gwin/gwin_button.c \
../ugfx/src/gwin/gwin_checkbox.c \
../ugfx/src/gwin/gwin_console.c \
../ugfx/src/gwin/gwin_container.c \
../ugfx/src/gwin/gwin_frame.c \
../ugfx/src/gwin/gwin_gl3d.c \
../ugfx/src/gwin/gwin_graph.c \
../ugfx/src/gwin/gwin_image.c \
../ugfx/src/gwin/gwin_keyboard.c \
../ugfx/src/gwin/gwin_keyboard_layout.c \
../ugfx/src/gwin/gwin_label.c \
../ugfx/src/gwin/gwin_list.c \
../ugfx/src/gwin/gwin_mk.c \
../ugfx/src/gwin/gwin_progressbar.c \
../ugfx/src/gwin/gwin_radio.c \
../ugfx/src/gwin/gwin_slider.c \
../ugfx/src/gwin/gwin_tabset.c \
../ugfx/src/gwin/gwin_textedit.c \
../ugfx/src/gwin/gwin_widget.c \
../ugfx/src/gwin/gwin_wm.c 

OBJS += \
./ugfx/src/gwin/gwin.o \
./ugfx/src/gwin/gwin_button.o \
./ugfx/src/gwin/gwin_checkbox.o \
./ugfx/src/gwin/gwin_console.o \
./ugfx/src/gwin/gwin_container.o \
./ugfx/src/gwin/gwin_frame.o \
./ugfx/src/gwin/gwin_gl3d.o \
./ugfx/src/gwin/gwin_graph.o \
./ugfx/src/gwin/gwin_image.o \
./ugfx/src/gwin/gwin_keyboard.o \
./ugfx/src/gwin/gwin_keyboard_layout.o \
./ugfx/src/gwin/gwin_label.o \
./ugfx/src/gwin/gwin_list.o \
./ugfx/src/gwin/gwin_mk.o \
./ugfx/src/gwin/gwin_progressbar.o \
./ugfx/src/gwin/gwin_radio.o \
./ugfx/src/gwin/gwin_slider.o \
./ugfx/src/gwin/gwin_tabset.o \
./ugfx/src/gwin/gwin_textedit.o \
./ugfx/src/gwin/gwin_widget.o \
./ugfx/src/gwin/gwin_wm.o 

C_DEPS += \
./ugfx/src/gwin/gwin.d \
./ugfx/src/gwin/gwin_button.d \
./ugfx/src/gwin/gwin_checkbox.d \
./ugfx/src/gwin/gwin_console.d \
./ugfx/src/gwin/gwin_container.d \
./ugfx/src/gwin/gwin_frame.d \
./ugfx/src/gwin/gwin_gl3d.d \
./ugfx/src/gwin/gwin_graph.d \
./ugfx/src/gwin/gwin_image.d \
./ugfx/src/gwin/gwin_keyboard.d \
./ugfx/src/gwin/gwin_keyboard_layout.d \
./ugfx/src/gwin/gwin_label.d \
./ugfx/src/gwin/gwin_list.d \
./ugfx/src/gwin/gwin_mk.d \
./ugfx/src/gwin/gwin_progressbar.d \
./ugfx/src/gwin/gwin_radio.d \
./ugfx/src/gwin/gwin_slider.d \
./ugfx/src/gwin/gwin_tabset.d \
./ugfx/src/gwin/gwin_textedit.d \
./ugfx/src/gwin/gwin_widget.d \
./ugfx/src/gwin/gwin_wm.d 


# Each subdirectory must supply rules for building sources it contributes
ugfx/src/gwin/%.o: ../ugfx/src/gwin/%.c ugfx/src/gwin/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"/home/lgiordano/workspace/cube_tests/sveltia_3/ugfx" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

