################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ugfx/src/gdisp/gdisp.c \
../ugfx/src/gdisp/gdisp_fonts.c \
../ugfx/src/gdisp/gdisp_image.c \
../ugfx/src/gdisp/gdisp_image_bmp.c \
../ugfx/src/gdisp/gdisp_image_gif.c \
../ugfx/src/gdisp/gdisp_image_jpg.c \
../ugfx/src/gdisp/gdisp_image_native.c \
../ugfx/src/gdisp/gdisp_image_png.c \
../ugfx/src/gdisp/gdisp_mk.c \
../ugfx/src/gdisp/gdisp_pixmap.c 

OBJS += \
./ugfx/src/gdisp/gdisp.o \
./ugfx/src/gdisp/gdisp_fonts.o \
./ugfx/src/gdisp/gdisp_image.o \
./ugfx/src/gdisp/gdisp_image_bmp.o \
./ugfx/src/gdisp/gdisp_image_gif.o \
./ugfx/src/gdisp/gdisp_image_jpg.o \
./ugfx/src/gdisp/gdisp_image_native.o \
./ugfx/src/gdisp/gdisp_image_png.o \
./ugfx/src/gdisp/gdisp_mk.o \
./ugfx/src/gdisp/gdisp_pixmap.o 

C_DEPS += \
./ugfx/src/gdisp/gdisp.d \
./ugfx/src/gdisp/gdisp_fonts.d \
./ugfx/src/gdisp/gdisp_image.d \
./ugfx/src/gdisp/gdisp_image_bmp.d \
./ugfx/src/gdisp/gdisp_image_gif.d \
./ugfx/src/gdisp/gdisp_image_jpg.d \
./ugfx/src/gdisp/gdisp_image_native.d \
./ugfx/src/gdisp/gdisp_image_png.d \
./ugfx/src/gdisp/gdisp_mk.d \
./ugfx/src/gdisp/gdisp_pixmap.d 


# Each subdirectory must supply rules for building sources it contributes
ugfx/src/gdisp/%.o: ../ugfx/src/gdisp/%.c ugfx/src/gdisp/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"/home/lgiordano/workspace/cube_tests/sveltia_3/ugfx" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

