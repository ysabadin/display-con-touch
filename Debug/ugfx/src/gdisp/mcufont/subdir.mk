################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ugfx/src/gdisp/mcufont/mf_bwfont.c \
../ugfx/src/gdisp/mcufont/mf_encoding.c \
../ugfx/src/gdisp/mcufont/mf_font.c \
../ugfx/src/gdisp/mcufont/mf_justify.c \
../ugfx/src/gdisp/mcufont/mf_kerning.c \
../ugfx/src/gdisp/mcufont/mf_rlefont.c \
../ugfx/src/gdisp/mcufont/mf_scaledfont.c \
../ugfx/src/gdisp/mcufont/mf_wordwrap.c 

OBJS += \
./ugfx/src/gdisp/mcufont/mf_bwfont.o \
./ugfx/src/gdisp/mcufont/mf_encoding.o \
./ugfx/src/gdisp/mcufont/mf_font.o \
./ugfx/src/gdisp/mcufont/mf_justify.o \
./ugfx/src/gdisp/mcufont/mf_kerning.o \
./ugfx/src/gdisp/mcufont/mf_rlefont.o \
./ugfx/src/gdisp/mcufont/mf_scaledfont.o \
./ugfx/src/gdisp/mcufont/mf_wordwrap.o 

C_DEPS += \
./ugfx/src/gdisp/mcufont/mf_bwfont.d \
./ugfx/src/gdisp/mcufont/mf_encoding.d \
./ugfx/src/gdisp/mcufont/mf_font.d \
./ugfx/src/gdisp/mcufont/mf_justify.d \
./ugfx/src/gdisp/mcufont/mf_kerning.d \
./ugfx/src/gdisp/mcufont/mf_rlefont.d \
./ugfx/src/gdisp/mcufont/mf_scaledfont.d \
./ugfx/src/gdisp/mcufont/mf_wordwrap.d 


# Each subdirectory must supply rules for building sources it contributes
ugfx/src/gdisp/mcufont/%.o: ../ugfx/src/gdisp/mcufont/%.c ugfx/src/gdisp/mcufont/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"/home/lgiordano/workspace/cube_tests/sveltia_3/ugfx" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

