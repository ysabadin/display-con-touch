################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ugfx/src/gqueue/gqueue.c \
../ugfx/src/gqueue/gqueue_mk.c 

OBJS += \
./ugfx/src/gqueue/gqueue.o \
./ugfx/src/gqueue/gqueue_mk.o 

C_DEPS += \
./ugfx/src/gqueue/gqueue.d \
./ugfx/src/gqueue/gqueue_mk.d 


# Each subdirectory must supply rules for building sources it contributes
ugfx/src/gqueue/%.o: ../ugfx/src/gqueue/%.c ugfx/src/gqueue/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"/home/lgiordano/workspace/cube_tests/sveltia_3/ugfx" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

