################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ugfx/drivers/gdisp/HX8347D/gdisp_lld_HX8347D.c 

OBJS += \
./ugfx/drivers/gdisp/HX8347D/gdisp_lld_HX8347D.o 

C_DEPS += \
./ugfx/drivers/gdisp/HX8347D/gdisp_lld_HX8347D.d 


# Each subdirectory must supply rules for building sources it contributes
ugfx/drivers/gdisp/HX8347D/%.o: ../ugfx/drivers/gdisp/HX8347D/%.c ugfx/drivers/gdisp/HX8347D/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"/home/lgiordano/workspace/cube_tests/sveltia_3/ugfx" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

