################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ugfx/drivers/gaudio/vs1053/gaudio_play_vs1053.c 

OBJS += \
./ugfx/drivers/gaudio/vs1053/gaudio_play_vs1053.o 

C_DEPS += \
./ugfx/drivers/gaudio/vs1053/gaudio_play_vs1053.d 


# Each subdirectory must supply rules for building sources it contributes
ugfx/drivers/gaudio/vs1053/%.o: ../ugfx/drivers/gaudio/vs1053/%.c ugfx/drivers/gaudio/vs1053/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"/home/lgiordano/workspace/cube_tests/sveltia_3/ugfx" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

