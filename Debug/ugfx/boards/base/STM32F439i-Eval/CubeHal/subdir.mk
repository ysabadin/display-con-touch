################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
S_SRCS += \
../ugfx/boards/base/STM32F439i-Eval/CubeHal/stm32f439i_raw32_startup.s 

C_SRCS += \
../ugfx/boards/base/STM32F439i-Eval/CubeHal/stm324x9i_eval_sdram.c \
../ugfx/boards/base/STM32F439i-Eval/CubeHal/stm32f439i_raw32_interrupts.c \
../ugfx/boards/base/STM32F439i-Eval/CubeHal/stm32f439i_raw32_system.c \
../ugfx/boards/base/STM32F439i-Eval/CubeHal/stm32f439i_raw32_ugfx.c 

OBJS += \
./ugfx/boards/base/STM32F439i-Eval/CubeHal/stm324x9i_eval_sdram.o \
./ugfx/boards/base/STM32F439i-Eval/CubeHal/stm32f439i_raw32_interrupts.o \
./ugfx/boards/base/STM32F439i-Eval/CubeHal/stm32f439i_raw32_startup.o \
./ugfx/boards/base/STM32F439i-Eval/CubeHal/stm32f439i_raw32_system.o \
./ugfx/boards/base/STM32F439i-Eval/CubeHal/stm32f439i_raw32_ugfx.o 

S_DEPS += \
./ugfx/boards/base/STM32F439i-Eval/CubeHal/stm32f439i_raw32_startup.d 

C_DEPS += \
./ugfx/boards/base/STM32F439i-Eval/CubeHal/stm324x9i_eval_sdram.d \
./ugfx/boards/base/STM32F439i-Eval/CubeHal/stm32f439i_raw32_interrupts.d \
./ugfx/boards/base/STM32F439i-Eval/CubeHal/stm32f439i_raw32_system.d \
./ugfx/boards/base/STM32F439i-Eval/CubeHal/stm32f439i_raw32_ugfx.d 


# Each subdirectory must supply rules for building sources it contributes
ugfx/boards/base/STM32F439i-Eval/CubeHal/%.o: ../ugfx/boards/base/STM32F439i-Eval/CubeHal/%.c ugfx/boards/base/STM32F439i-Eval/CubeHal/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"/home/lgiordano/workspace/cube_tests/sveltia_3/ugfx" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
ugfx/boards/base/STM32F439i-Eval/CubeHal/%.o: ../ugfx/boards/base/STM32F439i-Eval/CubeHal/%.s ugfx/boards/base/STM32F439i-Eval/CubeHal/subdir.mk
	arm-none-eabi-gcc -mcpu=cortex-m3 -g3 -DDEBUG -c -x assembler-with-cpp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@" "$<"

