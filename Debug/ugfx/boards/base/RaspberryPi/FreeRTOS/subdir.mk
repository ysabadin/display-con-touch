################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
S_SRCS += \
../ugfx/boards/base/RaspberryPi/FreeRTOS/startup.s 

C_SRCS += \
../ugfx/boards/base/RaspberryPi/FreeRTOS/freertos_main.c \
../ugfx/boards/base/RaspberryPi/FreeRTOS/gpio.c \
../ugfx/boards/base/RaspberryPi/FreeRTOS/interrupts.c \
../ugfx/boards/base/RaspberryPi/FreeRTOS/uart.c 

OBJS += \
./ugfx/boards/base/RaspberryPi/FreeRTOS/freertos_main.o \
./ugfx/boards/base/RaspberryPi/FreeRTOS/gpio.o \
./ugfx/boards/base/RaspberryPi/FreeRTOS/interrupts.o \
./ugfx/boards/base/RaspberryPi/FreeRTOS/startup.o \
./ugfx/boards/base/RaspberryPi/FreeRTOS/uart.o 

S_DEPS += \
./ugfx/boards/base/RaspberryPi/FreeRTOS/startup.d 

C_DEPS += \
./ugfx/boards/base/RaspberryPi/FreeRTOS/freertos_main.d \
./ugfx/boards/base/RaspberryPi/FreeRTOS/gpio.d \
./ugfx/boards/base/RaspberryPi/FreeRTOS/interrupts.d \
./ugfx/boards/base/RaspberryPi/FreeRTOS/uart.d 


# Each subdirectory must supply rules for building sources it contributes
ugfx/boards/base/RaspberryPi/FreeRTOS/%.o: ../ugfx/boards/base/RaspberryPi/FreeRTOS/%.c ugfx/boards/base/RaspberryPi/FreeRTOS/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F103xB -c -I../Core/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I"/home/lgiordano/workspace/cube_tests/sveltia_3/ugfx" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
ugfx/boards/base/RaspberryPi/FreeRTOS/%.o: ../ugfx/boards/base/RaspberryPi/FreeRTOS/%.s ugfx/boards/base/RaspberryPi/FreeRTOS/subdir.mk
	arm-none-eabi-gcc -mcpu=cortex-m3 -g3 -DDEBUG -c -x assembler-with-cpp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@" "$<"

